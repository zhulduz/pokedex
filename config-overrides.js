// https://medium.com/@timarney/but-i-dont-wanna-eject-3e3da5826e39

module.exports = function override(config, env) {

  // https://github.com/timarney/react-app-rewired/issues/46
  const babelOptions = config.module.rules.find(conf => {
    return conf.loader && conf.loader.includes('babel-loader')
  }).options
  const babelrc = require(babelOptions.presets[0])
  babelrc.plugins = [
    ['import', { libraryName: 'antd', style: 'css' }],
  ].concat(babelrc.plugins || [])
  babelOptions.presets = babelrc

  return config
}
