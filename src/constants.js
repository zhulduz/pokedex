// @flow

export const PROTOCOL = 'http'
export const HOST = 'pokeapi.co/api/v2'
export const ENDPOINT = `${PROTOCOL}://${HOST}`
