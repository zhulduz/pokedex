// @flow
import { ENDPOINT } from './constants'

export const getPokemon = ({offset = 20, limit}: Object) => {
  return fetch(`${ENDPOINT}/pokemon/?limit=${limit}&offset=${offset}`)
    .then((response) => {
      if (response.status === 200) {
        return response.json()
      }
      throw Error(response.statusText)
    })
}

export const getPokemonItem = ({url, id}: Object) => {
  url = url || `${ENDPOINT}/pokemon/${id}`
  return fetch(url)
    .then((response) => {
      if (response.status === 200) {
        return response.json()
      }
      throw Error(response.statusText)
    })
}

export const getTypes = () => {
  return fetch(`${ENDPOINT}/type/`)
    .then((response) => {
      if (response.status === 200) {
        return response.json()
      }
      throw Error(response.statusText)
    })
}

export const getPokemons = () => {
  return fetch('data.json')
    .then((response) => {
      return response.json()
    })
    .catch((error) => console.error(error))
}
