import { types } from '../types'
import {
  FETCH_TYPES_REQUEST,
  FETCH_TYPES_SUCCESS,
  FETCH_TYPES_ERROR
} from '../../actions/types'

describe('Reducer: types', () => {
  it('should return initial type state', () => {
    expect(types(undefined, {})).toMatchSnapshot()
  })

  it('should handle FETCH_TYPES_REQUEST', () => {
    const action = {
      type: FETCH_TYPES_REQUEST
    }
    expect(types([], action)).toMatchSnapshot()
  })

  it('should handle FETCH_TYPES_SUCCESS', () => {
    const action = {
      type: FETCH_TYPES_SUCCESS,
      data: [{
        name: 'normal',
        url: 'http://pokeapi.co/api/v2/type/1/'
      }]
    }
    expect(types([], action)).toMatchSnapshot()
  })

  it('should handle FETCH_TYPES_ERROR', () => {
    const action = {
      type: FETCH_TYPES_ERROR,
      errors: 'Error'
    }
    expect(types([], action)).toMatchSnapshot()
  })
})
