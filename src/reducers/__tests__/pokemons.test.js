import { filters, pokemons } from '../pokemons'
import {
  FETCH_POKEMONS_REQUEST,
  FETCH_POKEMONS_SUCCESS,
  FETCH_POKEMONS_ERROR,
  SEARCH_FILTER
} from '../../actions/pokemons'

describe('Reducer: pokemons', () => {
  it('should return initial pokemon state', () => {
    expect(pokemons(undefined, {})).toMatchSnapshot()
  })

  it('should handle FETCH_POKEMONS_REQUEST', () => {
    const action = {
      type: FETCH_POKEMONS_REQUEST
    }
    expect(pokemons([], action)).toMatchSnapshot()
  })

  it('should handle FETCH_POKEMONS_SUCCESS', () => {
    const action = {
      type: FETCH_POKEMONS_SUCCESS,
      data: [{
        name: 'spearow',
        order: 27,
        sprites: {},
        types: []
      }]
    }
    expect(pokemons([], action)).toMatchSnapshot()
  })

  it('should handle FETCH_POKEMONS_ERROR', () => {
    const action = {
      type: FETCH_POKEMONS_ERROR,
      errors: 'Error'
    }
    expect(pokemons([], action)).toMatchSnapshot()
  })
})

describe('Reducer: filter pokemons', () => {
  it('should return initial filter state', () => {
    expect(filters(undefined, {})).toMatchSnapshot()
  })

  it('should handle SEARCH_FILTER', () => {
    const action = {
      type: SEARCH_FILTER,
      searchValue: 'searchValue'
    }
    expect(filters([], action)).toMatchSnapshot()
  })
})
