// @flow
import typeToReducer from 'type-to-reducer'

import {
  FETCH_TYPES_REQUEST,
  FETCH_TYPES_SUCCESS,
  FETCH_TYPES_ERROR
} from '../actions/types'

import type { TypeState } from '../types'

const initialTypes: TypeState = {
  isLoading: false,
  isSuccess: false,
  isError: false,
  errors: [],
  data: []
}

export const types = typeToReducer({
  [FETCH_TYPES_REQUEST]: (state): TypeState => ({
    ...state,
    isLoading: true
  }),

  [FETCH_TYPES_SUCCESS]: (state, action): TypeState => ({
    ...state,
    isLoading: false,
    data: action.data,
    isSuccess: true,
    isError: false
  }),

  [FETCH_TYPES_ERROR]: (state, action): TypeState => ({
    ...state,
    isLoading: false,
    errors: action.errors,
    isSuccess: false,
    isError: true
  })

}, initialTypes)
