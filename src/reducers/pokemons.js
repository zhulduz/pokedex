// @flow
import typeToReducer from 'type-to-reducer'

import {
  FETCH_POKEMONS_REQUEST,
  FETCH_POKEMONS_SUCCESS,
  FETCH_POKEMONS_ERROR,
  SEARCH_FILTER
} from '../actions/pokemons'

import type { FilterState, PokemonState } from '../types'

const initialFilters: FilterState = {
  searchValue: ''
}

const initialPokemons: PokemonState = {
  isLoading: false,
  isSuccess: false,
  isError: false,
  errors: [],
  data: []
}

export const filters = typeToReducer({
  [SEARCH_FILTER]: (state, action): FilterState => ({
    ...state,
    searchValue: action.searchValue
  })

}, initialFilters)

export const pokemons = typeToReducer({
  [FETCH_POKEMONS_REQUEST]: (state): PokemonState => ({
    ...state,
    isLoading: true
  }),

  [FETCH_POKEMONS_SUCCESS]: (state, action): PokemonState => ({
    ...state,
    isLoading: false,
    data: action.data,
    isSuccess: true,
    isError: false
  }),

  [FETCH_POKEMONS_ERROR]: (state, action): PokemonState => ({
    ...state,
    isLoading: false,
    errors: action.errors,
    isSuccess: false,
    isError: true
  })

}, initialPokemons)
