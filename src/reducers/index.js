// @flow
import { combineReducers } from 'redux'

import { filters, pokemons } from './pokemons'
import { types } from './types'

const rootReducer = combineReducers({
  filters,
  pokemons,
  types
})

export default rootReducer
