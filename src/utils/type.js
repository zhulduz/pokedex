// @flow
import { join, map, path, pipe, any, equals } from 'ramda'

import type { Pokemon, TypeItem, Type } from '../types'

export const typeString = (types: TypeItem[]) => (
  pipe(
    map(
      (type) => {
        return path(['type', 'name'], type)
      }
    ),
    join(', ')
  )(types)
)

export const prepareFilters = (types: Type[]) => (
  map((type) => ({
    text: type.name,
    value: type.name
  }))(types)
)

export const onStatusFilter = (type: string, pokemon: Pokemon) => (
  any((item) => (
    equals(
      path(['type', 'name'], item),
      type
      )
    )
  )(pokemon.types)
)
