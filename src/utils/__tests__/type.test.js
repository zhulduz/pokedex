import { typeString, prepareFilters, onStatusFilter } from '../type'

describe('Utils: typeString', () => {
  const types = [
    {
      slot: 2,
      type: {
        name: 'ground',
        url: 'http://pokeapi.co/api/v2/type/5/'
      }
    },
    {
      slot: 1,
      type: {
        name: 'normal',
        url: 'http://pokeapi.co/api/v2/type/2/'
      }
    }
  ]

  test('should return string with names', () => {
    expect(typeString(types)).toMatchSnapshot()
  })
})

describe('Utils: prepareFilters', () => {
  const types = [
    {
      name: 'poison',
      url: 'http://pokeapi.co/api/v2/type/4/'
    },
    {
      name: 'normal',
      url: 'http://pokeapi.co/api/v2/type/2/'
    }
  ]

  test('should return object with proper text and value', () => {
    expect(prepareFilters(types)).toMatchSnapshot()
  })
})

describe('Utils: onStatusFilter', () => {
  const pokemon = {
    name: 'nidoqueen',
    order: 27,
    sprites: {
      front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/35.png'
    },
    types: [
      {
        slot: 2,
        type: {
          name: 'ground',
          url: 'http://pokeapi.co/api/v2/type/5/'
        }
      },
      {
        slot: 1,
        type: {
          name: 'normal',
          url: 'http://pokeapi.co/api/v2/type/2/'
        }
      }
    ]
  }

  test('should return object with proper text and value', () => {
    expect(onStatusFilter('normal', pokemon)).toMatchSnapshot()
  })
})
