import { searchItem } from '../search'

describe('Utils: searchItem', () => {
  const data = [
    {name: 'spearow'},
    {name: 'fearow'},
    {name: 'raichu'},
    {name: 'beedrill-mega'},
    {name: 'swampert-mega'},
    {name: 'barbaracle'}
  ]

  test('should return all data', () => {
    expect(searchItem(data, '')).toEqual(data)
  })

  test('should return empty array', () => {
    expect(searchItem(data, 'xyz')).toEqual([])
  })

  test('should filter', () => {
    const expectedData = [
      {name: 'spearow'},
      {name: 'fearow'}
    ]
    expect(searchItem(data, 'ea')).toEqual(expectedData)
  })
})
