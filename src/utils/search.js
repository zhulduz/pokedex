// @flow
import { isEmpty, filter, contains } from 'ramda'

export const searchItem = (data: Object, searchValue: string): Object => {
  if (isEmpty(searchValue)) {
    return data
  } else {
    return filter((item) => contains(searchValue, item.name), data)
  }
}
