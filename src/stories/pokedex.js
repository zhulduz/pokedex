// @flow
import React from 'react'
import { storiesOf } from '@storybook/react'
import { LocaleProvider } from 'antd'
import enUS from 'antd/lib/locale-provider/en_US'
import 'antd/dist/antd.css'

import PokemonTable from '../components/PokemonTable'
import type { Pokemon, Type } from '../types'

const pokemons: Pokemon[] = [
  {
    name: 'spearow',
    order: 27,
    sprites: {
      front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/35.png'
    },
    types: [
      {
        slot: 2,
        type: {
          name: 'flying',
          url: 'http://pokeapi.co/api/v2/type/3/'
        }
      },
      {
        slot: 1,
        type: {
          name: 'normal',
          url: 'http://pokeapi.co/api/v2/type/2/'
        }
      }
    ]
  },
  {
    name: 'nidoqueen',
    order: 27,
    sprites: {
      front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/35.png'
    },
    types: [
      {
        slot: 2,
        type: {
          name: 'ground',
          url: 'http://pokeapi.co/api/v2/type/5/'
        }
      },
      {
        slot: 1,
        type: {
          name: 'normal',
          url: 'http://pokeapi.co/api/v2/type/2/'
        }
      }
    ]
  },
  {
    name: 'nidorino',
    order: 27,
    sprites: {
      front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/35.png'
    },
    types: [
      {
        slot: 1,
        type: {
          name: 'poison',
          url: 'http://pokeapi.co/api/v2/type/4/'
        }
      }
    ]
  }
]

const types: Type[] = [
  {
    name: 'poison',
    url: 'http://pokeapi.co/api/v2/type/4/'
  },
  {
    name: 'normal',
    url: 'http://pokeapi.co/api/v2/type/2/'
  },
  {
    name: 'ground',
    url: 'http://pokeapi.co/api/v2/type/5/'
  },
  {
    name: 'flying',
    url: 'http://pokeapi.co/api/v2/type/3/'
  }
]

storiesOf('Pokedex', module)
  .add('Table', () => (
    <LocaleProvider locale={enUS}>
      <PokemonTable
        data={pokemons}
        types={types}
      />
    </LocaleProvider>
  ))
