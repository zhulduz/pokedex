// @flow
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { LocaleProvider } from 'antd'
import enUS from 'antd/lib/locale-provider/en_US'

import PokemonView from './PokemonView'
import store from '../store'

class App extends Component {
  render () {
    return (
      <LocaleProvider locale={enUS}>
        <Provider store={store}>
          <PokemonView />
        </Provider>
      </LocaleProvider>
    )
  }
}

export default App
