// @flow
import React, { Component } from 'react'
import { connect } from 'react-redux'

import PokemonTable from '../components/PokemonTable'
import { fetchPokemons, searchFilter } from '../actions/pokemons'
import { fetchTypes } from '../actions/types'

import { searchItem } from '../utils/search'

class PokemonView extends Component {
  componentDidMount () {
    this.props.dispatch(fetchPokemons())
    this.props.dispatch(fetchTypes())
  }

  handleSearch = (searchValue) => {
    this.props.dispatch(searchFilter(searchValue))
  }

  render () {
    const {
      data,
      types,
      isLoading
    } = this.props

    return (
      <PokemonTable
        data={data}
        types={types}
        isLoading={isLoading}
        handleSearch={this.handleSearch}
      />
    )
  }
}

const filterData = (pokemons, filters) => {
  return searchItem(pokemons.data, filters.searchValue)
}

const mapStateToProps = state => ({
  data: filterData(state.pokemons, state.filters),
  types: state.types.data,
  isLoading: state.pokemons.isLoading
})

export default connect(
  mapStateToProps
)(PokemonView)
