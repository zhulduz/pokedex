// eslint-disable-next-line fp/no-mutation
window.matchMedia = window.matchMedia ||
  function () { // eslint-disable-line func-names
    return {
      matches: false,
      addListener: function () {},
      removeListener: function () {}
    }
  }
