// @flow
export type PromiseState = {
  isLoading: boolean,
  isSuccess: boolean,
  isError: boolean,
  errors: any,
}

export type Type = {|
  name: string,
  url: string
|}

export type TypeItem = {|
  slot: number,
  type: Type
|}

export type Sprites = {
  front_default: string
}

export type Pokemon = {
  name: string,
  height: number,
  order: number,
  sprites: Sprites,
  types: TypeItem[]
}

export type TypeState = PromiseState & { data: ?Type[] }

export type PokemonState = PromiseState & { data: ?Pokemon[] }

export type FilterState = {|
  searchValue: string
|}
