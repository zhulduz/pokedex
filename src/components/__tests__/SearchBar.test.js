import React from 'react'
import renderer from 'react-test-renderer'

import SearchBar from '../SearchBar'

test('SearchBar', () => {
  const component = renderer.create(
      <SearchBar />
  )
  const tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})
