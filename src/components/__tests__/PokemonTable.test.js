import React from 'react'
import { mount } from 'enzyme'
import toJson from 'enzyme-to-json'

import PokemonTable from '../PokemonTable'

const data = [{
  id: 1,
  name: 'spearow',
  order: 27,
  sprites: {
    front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/35.png'
  },
  types: [
    {
      slot: 2,
      type: {
        name: 'flying',
        url: 'http://pokeapi.co/api/v2/type/3/'
      }
    },
    {
      slot: 1,
      type: {
        name: 'normal',
        url: 'http://pokeapi.co/api/v2/type/2/'
      }
    }
  ]
}]

const types = [{
  name: 'poison',
  url: 'http://pokeapi.co/api/v2/type/4/'
}]
const isLoading = true
const handleSearch = () => {}
const className = 'className'

test('PokemonTable', () => {
  const component = mount(
    <PokemonTable
      data={data}
      types={types}
      isLoading={isLoading}
      handleSearch={handleSearch}
      className={className}
    />
  )

  const tree = toJson(component)
  expect(tree).toMatchSnapshot('SalesPage')
})
