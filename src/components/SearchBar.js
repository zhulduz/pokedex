// @flow
import React, { Component } from 'react'

import { Form, Input } from 'antd'

const searchStyle = { width: 200 }

type Props = {
  onSearch: (string) => void,
  className: string
}

class SearchBar extends Component {
  props: Props

  static defaultProps = {
    className: 'pokemon-search'
  }

  handleChangeSearch = ({ currentTarget }: any) => {
    this.props.onSearch(currentTarget.value)
  }

  render () {
    const {
      onSearch,
      className
    } = this.props
    return (
      <Form layout="inline" className={className}>
        <Form.Item>
          <Input.Search
            placeholder="Search..."
            style={searchStyle}
            onSearch={onSearch}
            onChange={this.handleChangeSearch}
          />
        </Form.Item>
      </Form>
    )
  }
}

export default SearchBar
