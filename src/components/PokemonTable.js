// @flow
import React from 'react'
import { Table, Avatar, Spin } from 'antd'

import SearchBar from './SearchBar'

import { typeString, prepareFilters, onStatusFilter } from '../utils/type'
import type { Pokemon, Type, TypeItem } from '../types'

const { Column } = Table

type Props = {
  data: Pokemon,
  types: Type[],
  isLoading: boolean,
  handleSearch: (string) => void,
  className: string
}

class PokemonTable extends React.Component {
  props: Props

  static defaultProps = {
    className: 'pokemon-table'
  }

  handleSearch = (searchValue: string) => {
    this.props.handleSearch(searchValue)
  }

  render () {
    const {
      data,
      types,
      isLoading,
      className
    } = this.props

    const avatarRender = (url: string) => <Avatar src={url} />

    const typesRender = (types: TypeItem[]) => (
      <span>{typeString(types)}</span>
    )

    const paginationConfig = () => ({
      showSizeChanger: true
    })

    return (
      <div>
        <SearchBar
          onSearch={this.handleSearch}
        />
        <div className={className}>
          {isLoading &&
            <Spin size="large" />
          }
          {!isLoading &&
            <Table
              dataSource={data}
              pagination={paginationConfig()}
              rowKey='id'
            >
              <Column
                title='Avatar'
                dataIndex='sprites.front_default'
                render={avatarRender}
              />
              <Column
                title='Name'
                dataIndex='name'
              />
              <Column
                title='Type'
                dataIndex='types'
                render={typesRender}
                filters={prepareFilters(types)}
                onFilter={onStatusFilter}
              />
            </Table>
          }
        </div>
      </div>
    )
  }
}

export default PokemonTable
