// @flow
import { prop } from 'ramda'

import { getTypes } from '../api'

export const FETCH_TYPES_REQUEST = 'FETCH_TYPES_REQUEST'
export const FETCH_TYPES_SUCCESS = 'FETCH_TYPES_SUCCESS'
export const FETCH_TYPES_ERROR = 'FETCH_TYPES_ERROR'

const requestTypes = () => ({
  type: FETCH_TYPES_REQUEST
})

const receiveTypes = (data) => ({
  type: FETCH_TYPES_SUCCESS,
  data
})

const receiveError = (errors) => ({
  type: FETCH_TYPES_ERROR,
  errors: errors
})

export function fetchTypes () {
  return async function (dispatch: Function) {
    dispatch(requestTypes())
    try {
      const types = await getTypes()
      dispatch(receiveTypes(prop('results', types)))
    } catch (error) {
      dispatch(receiveError(error))
    }
  }
}
