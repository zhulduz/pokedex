// @flow
import { getPokemons } from '../api'

export const FETCH_POKEMONS_REQUEST = 'FETCH_POKEMONS_REQUEST'
export const FETCH_POKEMONS_SUCCESS = 'FETCH_POKEMONS_SUCCESS'
export const FETCH_POKEMONS_ERROR = 'FETCH_POKEMONS_ERROR'
export const SEARCH_FILTER = 'SEARCH_FILTER'

const requestPokemons = () => ({
  type: FETCH_POKEMONS_REQUEST
})

const receivePokemons = (data) => ({
  type: FETCH_POKEMONS_SUCCESS,
  data
})

const receiveError = (errors: Object) => ({
  type: FETCH_POKEMONS_ERROR,
  errors: errors
})

async function makeRequest () {
  const pokemons = await getPokemons()
  return pokemons
}

export const searchFilter = (searchValue: string) => ({
  type: SEARCH_FILTER,
  searchValue
})

export function fetchPokemons () {
  return async function (dispatch: Function) {
    dispatch(requestPokemons())
    try {
      const pokemons = await makeRequest()
      dispatch(receivePokemons(pokemons))
    } catch (error) {
      dispatch(receiveError(error))
    }
  }
}
