import { SEARCH_FILTER, searchFilter } from '../pokemons'

describe('Action: pokemons', () => {
  it('search filter', () => {
    const searchValue = 'pokemon'
    const expectedAction = {
      type: SEARCH_FILTER,
      searchValue
    }

    expect(searchFilter(searchValue)).toEqual(expectedAction)
  })
})
