import configureStore from 'redux-mock-store'
import thunkMiddleware from 'redux-thunk'
import fetchMock from 'fetch-mock'

import { fetchTypes } from '../types'

const mockStore = configureStore([
  thunkMiddleware
])

describe('Action: types', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  test('fetch types: success', async () => {
    const response = {
      status: 200,
      body: {
        results: [{
          name: 'normal',
          url: 'http://pokeapi.co/api/v2/type/1/'
        }]
      }
    }

    fetchMock.get('end:/type/', response)
    const store = mockStore({})

    await store.dispatch(fetchTypes())

    const actualActions = store.getActions()
    expect(actualActions).toMatchSnapshot()
  })

  test('fetch types: error', async () => {
    const response = {
      status: 400,
      body: {
        error: 'Bad request'
      }
    }

    fetchMock.get('end:/type/', response)
    const store = mockStore({})

    await store.dispatch(fetchTypes())

    const actualActions = store.getActions()
    expect(actualActions).toMatchSnapshot()
  })
})
