import { configure } from '@storybook/react'

function loadStories() {
  require('../src/stories/pokedex.js')
}

configure(loadStories, module)
